package Lesson2;

public class Task5 {
    public static void main(String[] args) {
        {
            int count = 0;
            String str = "сегодня мы купили 1 яблоко, 1 грушу и 2 апельсина";
            System.out.println(str);
            for (int i = 0; i < str.length(); i++)
                if (Character.isDigit(str.charAt(i))) count++;
            System.out.println("посчитать цифры в строке " + count);
        }
    }
}
//  Дано: строка str, начальное значение может быть любое.
//Напишите программу, считающую количество цифр 1, 2, 3 в строке.
///Пример: str = "сегодня мы купили 1 яблоко, 1 грушу и 2 апельсина"
///Кол-во 1: 2
////Кол-во 2: 1
////Кол-во 3: 0

