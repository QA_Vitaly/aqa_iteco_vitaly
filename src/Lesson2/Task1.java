package Lesson2;

//Дано: переменная int count, начальное значение можно указать любое.
//Напишите программу, которая выводит на экран count в степени 10,
// если count является чётным числом, и count в степени 3, если count нечётное.
public class Task1 {
    public static void main(String[] args) {
        int count = 3;
        if (count % 2 == 0) {
            System.out.println(" Count even  " + Math.pow(count, 10) + "  - четное в 10 степени");
        } else {
            {
                System.out.println("Count odd  " + Math.pow(count, 3) + "  - нечетное  в степени 3");
            }
        }
    }
}
