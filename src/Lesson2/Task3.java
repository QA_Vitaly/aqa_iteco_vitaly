package Lesson2;

//Дано: строка str,  начальное значение можно указать любое.
// Напишите программу, которая удаляет в строке все лишние пробелы,
//  то есть серии подряд идущих пробелов заменяет на одиночные пробелы.
//  Крайние пробелы в строке также должны удалиться.
// Пример: str = " привет,   в этой    строке лишние   пробелы.  "
//  Результат: "привет, в этой строке лишние пробелы"
public class Task3 {
    public static void main(String[] args) {
        String str = " привет,  в этой    строке лишние   пробелы.  ";
        str = str.replaceAll("\\s+", " ");
        System.out.println(str);
    }
}



