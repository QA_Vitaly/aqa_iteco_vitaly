package Lesson2;

public class Task4 {
    //Дано: целочисленное число n, начальное значение может быть любое.
    //Напишите программу, которая определяет: //а) количество цифр в нем;
    public static void main(String[] args) {
        int n = 1234567890;
        int cntNum = 0;
        for (; n != 0; n /= 10)
            ++cntNum;
        System.out.println(cntNum);
    }
}
