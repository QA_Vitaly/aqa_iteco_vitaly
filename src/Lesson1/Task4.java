package Lesson1;

public class Task4 {
    public static void main(String[] args) {
        //byte = 	от -128 до 127
        //short = от -32768 до 32767
        //int = от -2147483648 до 214748364
        // long = 	от -9223372036854775808L до 9223372036854775807L
        System.out.print("Byte ");
        System.out.print("MIN ");
        System.out.print(Byte.MIN_VALUE);
        System.out.print(" MAX ");
        System.out.println(Byte.MAX_VALUE);
        System.out.print("Short ");
        System.out.print(" MIN  ");
        System.out.print(Short.MIN_VALUE);
        System.out.print("MAX  ");
        System.out.println(Short.MAX_VALUE);
        System.out.print("Integer ");
        System.out.print("MIN ");
        System.out.print(Integer.MIN_VALUE);
        System.out.print("  MAX  ");
        System.out.println(Integer.MAX_VALUE);
        System.out.print("Long ");
        System.out.print("MIN ");
        System.out.print(Long.MIN_VALUE);
        System.out.print("  MAX  ");
        System.out.print(Long.MAX_VALUE);
    }
}
