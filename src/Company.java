public class Company {
    // public class Company { // создаем Внутренний вложенный класс "Company", если считаем что он больше нигде не понадобится.
    public static int INDEX_COMPANY = 12;// указан для всех и не зависит от конкретной имплементации
    private String name;
    private String description;
    private int companyIndex;

    public Company(String name, String description, int companyIndex) {
        this.name = name;
        this.description = description;
        this.companyIndex = companyIndex;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}

