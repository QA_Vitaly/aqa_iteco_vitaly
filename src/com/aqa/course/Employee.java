//package com.aqa.course;
//
//public class Employee {
//    private static final double DEFAULT_SALARY = 50;
//    private String name;// недоступно если private и видно всем если public
//    private double salary;
//    private Company company;
//    private int floor; // данные для программы
//
//    public Employee(String name, double salary, int roomNumber) { //сотрудники известные с зарплатой
//        this.name = name;
//        this.salary = salary;
//        this.company = new Company("Style", "New pretty awesome company",1);
//        this.floor = new OfficeRoom(roomNumber).getFloor();
//    }
//
//    public Employee(String name) {
//        this.name = name;
//        this.salary = DEFAULT_SALARY; //для сотрудников у которых не указана зарплата тот с defaultsalary
//    }
//
//    public String getName() {// спец метод для возврата текущего имени если класс private
//        return this.name;
//    }
//
//    public double getSalary() {// сгенерировать через getter если ранее уже был подобный метод
//        return salary;
//    }
//
//    public void raiseSalary(double newSalary) { // метод ничего не меняет поэтому void
//        this.salary = newSalary;
//    }
//
//    public Company getCompany() {
//        return company;
//    }
//
//    public int getFloor() {
//        return floor;
//    }
//
//    class OfficeRoom {
//        private int roomNumber;
//        private int floor;
//
//        public int getFloor() {
//            return roomNumber / 10;
//        }
//
//        public OfficeRoom(int roomNumber) {
//            this.roomNumber = roomNumber;
//        }
//    }
//}
